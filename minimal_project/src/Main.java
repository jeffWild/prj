import com.opencsv.CSVWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe main permettant de lancer une �x�cution
 * @author gsauvage
 */
public class Main {

    public static void main(String[] args) throws ParseException {

        GenerationEtLectureTexte.exploitationFichierNettoye("F:/fichierNettoye.txt", "F:/erreur_inconnue.csv","F:/erreur_interne.csv");
    }
}
