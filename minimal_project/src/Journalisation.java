import java.io.*;
import java.util.*;

/**
 * Created by gsauvage on 28/11/2017.
 */
public class Journalisation {

    public static void main() {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");

        List<String> types = new ArrayList<>();
        types.add("ADM");
        types.add("ALI");
        types.add("ALR");
        types.add("ANA");
        types.add("EXP");
        types.add("IDN");
        types.add("ORG");
        types.add("PHO");
        types.add("RAP");
        types.add("VEI");
        List<String> fichiersResultat = new ArrayList<>();
        String fichierResultat = "";
        for (String type : types) {
            File file = new File("D:/Profiles/gsauvage/Desktop/11099/" + type + ".txt");
            String texteEntier = GenerationEtLectureTexte.generateStringFromFile(file);
            String[] tableau = GenerationEtLectureTexte.splitStringOnChar(" ", texteEntier);
            List<String> lignes = recupererLignesString(tableau);
            Set<LigneJfoDto> LigneJfoDtoSet = recupererLignes(lignes);
            fichiersResultat.add(miseEnFormeTableau(LigneJfoDtoSet));
        }
        for (String texte : fichiersResultat) {
            fichierResultat += texte;
        }
        GenerationEtLectureTexte.generateFileFromString(fichierResultat, "D:/Profiles/gsauvage/Desktop/11099/fichier_resultat/resultat.txt");

    }

    public static String miseEnFormeTableau(Set<LigneJfoDto> LigneJfoDtoSet) {
        StringBuilder sb = new StringBuilder();
        for (LigneJfoDto LigneJfoDto : LigneJfoDtoSet) {
            sb.append(LigneJfoDto.getDMA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getFCA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getACA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getDNA_CODE());
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public static List<String> recupererLignesString(String[] tableau) {
        List<String> lignes = new ArrayList<>();
        int compteur = 1;
        StringBuilder ligne = new StringBuilder();
        for (int i = 0 ; i < tableau.length ; i++) {
            if (compteur % 5 == 0) {
                lignes.add(ligne.toString());
                ligne = new StringBuilder();
                compteur = 1;
                i--;
            } else {
                ligne.append(tableau[i]);
                ligne.append(" ");
                compteur++;
            }
        }
        return lignes;
    }


    public static Set<LigneJfoDto> recupererLignes(List<String> stringList) {
        Set<LigneJfoDto> LigneJfoDtos = new HashSet<>();
        for (String string : stringList) {
            LigneJfoDto LigneJfoDto = new LigneJfoDto();
            String[] tableau = string.split(" ");
            LigneJfoDto.setDMA_CODE(tableau[0]);
            LigneJfoDto.setFCA_CODE(tableau[1]);
            LigneJfoDto.setACA_CODE(tableau[2]);
            LigneJfoDto.setDNA_CODE(tableau[3]);
            LigneJfoDtos.add(LigneJfoDto);
        }
        return LigneJfoDtos;
    }

}


