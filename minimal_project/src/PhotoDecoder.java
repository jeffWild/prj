import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class PhotoDecoder {

    public static void main(String[] args) throws IOException {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");

        File file = new File("E:/photo_byte_rejet.txt");

        String texteEntier = GenerationEtLectureTexte.generateStringFromFile(file);

        byte[] photoByteArray = texteEntier.getBytes("UTF-8");
        byte[] photoByteArrayDecode = Base64.decode(texteEntier);
        ByteArrayInputStream bais = new ByteArrayInputStream(photoByteArrayDecode);
        BufferedImage bufferedImageSource = ImageIO.read(bais);
        int width = bufferedImageSource.getWidth();
        System.out.println("width : " + width);



    }

}

