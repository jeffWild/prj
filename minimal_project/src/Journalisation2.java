import java.io.*;
import java.util.*;

/**
 * Created by gsauvage on 28/11/2017.
 */
public class Journalisation2 {

    public static void main() {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");

        File file = new File("D:/Profiles/gsauvage/Desktop/entree.txt");
        String texteEntier = GenerationEtLectureTexte.generateStringFromFile(file);
        Set<LigneJfoDto> ligneJfoDtoSet = genererLignesFromTexte(texteEntier);
        GenerationEtLectureTexte.generateFileFromString(miseEnFormeTableau(ligneJfoDtoSet), "D:/Profiles/gsauvage/Desktop/resultat.txt");

    }

    public static String miseEnFormeTableau(Set<LigneJfoDto> LigneJfoDtoSet) {
        StringBuilder sb = new StringBuilder();
        for (LigneJfoDto LigneJfoDto : LigneJfoDtoSet) {
            sb.append(LigneJfoDto.getDMA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getFCA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getACA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getDNA_CODE());
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public static List<String> recupererLignesString(String[] tableau) {
        List<String> lignes = new ArrayList<>();
        int compteur = 1;
        StringBuilder ligne = new StringBuilder();
        for (int i = 0 ; i < tableau.length ; i++) {
            if (compteur % 5 == 0) {
                lignes.add(ligne.toString());
                ligne = new StringBuilder();
                compteur = 1;
                i--;
            } else {
                ligne.append(tableau[i]);
                ligne.append(" ");
                compteur++;
            }
        }
        return lignes;
    }


    public static Set<LigneJfoDto> genererLignes(List<String> stringList) {
        Set<LigneJfoDto> LigneJfoDtos = new HashSet<>();
        for (String string : stringList) {
            LigneJfoDto LigneJfoDto = new LigneJfoDto();
            String[] tableau = string.split("\t");
            LigneJfoDto.setDMA_CODE(tableau[0]);
            LigneJfoDto.setFCA_CODE(tableau[1]);
            LigneJfoDto.setACA_CODE(tableau[2]);
            LigneJfoDto.setDNA_CODE(tableau[3]);
            LigneJfoDtos.add(LigneJfoDto);
        }
        return LigneJfoDtos;
    }

    public static LigneJfoDto genererLigneFromTab(String[] tableau) {
        LigneJfoDto LigneJfoDto = new LigneJfoDto();
        LigneJfoDto.setDMA_CODE(tableau[0]);
        LigneJfoDto.setFCA_CODE(tableau[1]);
        LigneJfoDto.setACA_CODE(tableau[2]);
        LigneJfoDto.setDNA_CODE(tableau[3]);
        return LigneJfoDto;
    }

    public static Set<LigneJfoDto> genererLignesFromTexte(String texte){
        Set<LigneJfoDto> ligneJfoDtoSet = new HashSet<>();

        String[] tableau = GenerationEtLectureTexte.splitStringOnChar("\r\n", texte);
        for (String ligne : tableau) {
            String[] ligneDecoupeTab = GenerationEtLectureTexte.splitStringOnChar("\t", ligne);
            ligneJfoDtoSet.add(genererLigneFromTab(ligneDecoupeTab));
        }
        return ligneJfoDtoSet;
    }
}


