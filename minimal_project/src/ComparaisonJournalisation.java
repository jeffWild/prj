import java.io.File;
import java.util.*;

/**
 * Created by gsauvage on 05/12/2017.
 */
public class ComparaisonJournalisation {

    public static void main() {
        File fichierLigneUnique = new File("D:/Profiles/gsauvage/Desktop/11099/liste_fonctions_critere_unique.txt");
        File fichierLigneCritereUniqueOuMultiple = new File("D:/Profiles/gsauvage/Desktop/11099/liste_fonctions_generant_critere_unique_ou_multiple.txt");
        File fichierLigneDoubleProd = new File("D:/Profiles/gsauvage/Desktop/11099/liste_fonctions_double_prod.txt");

        //File fichierLigneCritereUniqueOuMultiple = new File("D:/Profiles/gsauvage/Desktop/test1.txt");
        //File fichierLigneDoubleProd = new File("D:/Profiles/gsauvage/Desktop/test2.txt");

        String ligneUniqueStr = GenerationEtLectureTexte.generateStringFromFile(fichierLigneUnique);
        String ligneCritereUniqueOuMultipleStr = GenerationEtLectureTexte.generateStringFromFile(fichierLigneCritereUniqueOuMultiple);
        String ligneDoubleProdStr = GenerationEtLectureTexte.generateStringFromFile(fichierLigneDoubleProd);

        Set<LigneJfoDto> ligneUniqueSet = LigneJfoDto.genererLignesFromTexte(ligneUniqueStr);
        enleverLibelle(ligneUniqueSet);
        Set<LigneJfoDto> ligneCritereUniqueOuMultipleSet = LigneJfoDto.genererLignesFromTexte(ligneCritereUniqueOuMultipleStr);
        enleverLibelle(ligneCritereUniqueOuMultipleSet);
        Set<LigneJfoDto> ligneDoubleProdSet = LigneJfoDto.genererLignesFromTexte(ligneDoubleProdStr);

        Map<String, Set<LigneJfoDto>> comparaisonLigneProdEtLigneCritereUniqueOuMultiple = compareSet(ligneDoubleProdSet, ligneCritereUniqueOuMultipleSet);
        // comparaison de la liste a priori ne générant pas de critère avec la liste ne générant qu'un seul critère
        Map<String, Set<LigneJfoDto>> comparaison2 = compareSet(comparaisonLigneProdEtLigneCritereUniqueOuMultiple.get("lignesUniquesSet1"), ligneUniqueSet);
        GenerationEtLectureTexte.generateFileFromString(LigneJfoDto.genererTexteFromLigneSet(comparaison2.get("lignesUniquesSet1")), "D:/Profiles/gsauvage/Desktop/11099/resultat.txt");

    }

    private static Map<String, Set<LigneJfoDto>> compareSet(Set<LigneJfoDto> set1, Set<LigneJfoDto> set2) {
        Map<String, Set<LigneJfoDto>> comparaisonMap = new HashMap<>();
        Set<LigneJfoDto> lignesCommunes = new HashSet<>();
        Set<LigneJfoDto> lignesUniquesSet1 = new HashSet<>();
        Set<LigneJfoDto> lignesUniquesSet2 = new HashSet<>();
        // 1ere boucle
        Iterator iteratorSet1 = set1.iterator();
        while (iteratorSet1.hasNext()) {
            LigneJfoDto ligneSet1 = (LigneJfoDto) iteratorSet1.next();
            Iterator iteratorSet2 = set2.iterator();
            boolean elementTrouve = false;
            while (iteratorSet2.hasNext()) {
                LigneJfoDto ligneSet2 = (LigneJfoDto) iteratorSet2.next();
                if (ligneSet1.getFCA_CODE().contentEquals(ligneSet2.getFCA_CODE()) && ligneSet1.getFCA_CODE().contentEquals(ligneSet2.getFCA_CODE())) {
                    elementTrouve = true;
                    break;
                }
            }
            if (elementTrouve == false) {
                lignesUniquesSet1.add(ligneSet1);
            } else {
                lignesCommunes.add(ligneSet1);
            }
        }
        // 2eme boucle
        Iterator iteratorSet22 = set2.iterator();
        while (iteratorSet22.hasNext()) {
            LigneJfoDto ligneSet22 = (LigneJfoDto) iteratorSet22.next();
            Iterator iteratorSet21 = set1.iterator();
            boolean elementTrouve = false;
            while (iteratorSet21.hasNext()) {
                LigneJfoDto ligneSet21 = (LigneJfoDto) iteratorSet21.next();
                if (ligneSet22.getFCA_CODE().contentEquals(ligneSet21.getFCA_CODE()) && ligneSet22.getFCA_CODE().contentEquals(ligneSet21.getFCA_CODE())) {
                    elementTrouve = true;
                    break;
                }
            }
            if (elementTrouve == false) {
                lignesUniquesSet2.add(ligneSet22);
            }
        }

        comparaisonMap.put("lignesCommunes", lignesCommunes);
        comparaisonMap.put("lignesUniquesSet1", lignesUniquesSet1);
        comparaisonMap.put("lignesUniquesSet2", lignesUniquesSet2);
        return comparaisonMap;
    }

    public static void enleverLibelle(Set<LigneJfoDto> ligneSet) {
        for (LigneJfoDto ligneJfoDto : ligneSet) {
            ligneJfoDto.setFCA_CODE(ligneJfoDto.getFCA_CODE().split(":")[0]);
            ligneJfoDto.setACA_CODE(ligneJfoDto.getACA_CODE().split(":")[0]);
        }
    }


}
