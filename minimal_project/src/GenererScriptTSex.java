import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenererScriptTSex {

    public static List<String > nomColonneList = new ArrayList<>();

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");

        File file = new File("D:/Profiles/gsauvage/Desktop/T_SEX_SECTION_EXPORT_201709071347.sql");

        String texteEntier = GenerationEtLectureTexte.generateStringFromFile(file);
        String[] requetes = GenerationEtLectureTexte.splitStringOnChar(";", texteEntier);
        StringBuilder stringSortie = new StringBuilder();
        nomColonneList.add("SEX_CODE");
        nomColonneList.add("SEX_TYPE_FICHE");
        nomColonneList.add("SEX_LIBELLE");
        nomColonneList.add("SEX_DESCRIPTION");
        nomColonneList.add("SEX_VISIBILITE");
        nomColonneList.add("SEX_HIERARCHIE");
        for (String ligne : requetes) {
            stringSortie.append(traiterLigne(ligne));
        }
        GenerationEtLectureTexte.generateFileFromString(stringSortie.toString(), "D:/Profiles/gsauvage/Desktop/fichier_traite.sql");

    }

    public static String traiterLigne(String ligneATraiter) {
        String[] valeurSplit = GenerationEtLectureTexte.splitStringOnChar("'", ligneATraiter);
        valeurSplit = enleverStringVide(valeurSplit);
        List<String> valeurs = new ArrayList<>();
        for (int i = 1 ; i <= 11 ; i = i + 2) {
            valeurs.add(valeurSplit[i]);
        }
        String retourStr = creerLigne(nomColonneList, valeurs, leftPadZeroAddOne(valeurSplit[11]));

        return retourStr;
    }

    public static String[] enleverStringVide(String[] tableauATraiter) {
        List<String> tabEntree = Arrays.asList(tableauATraiter);
        List<String> tabSortie = new ArrayList<>();
        int tailleTabEntree = tabEntree.size();
        int decalage = 1;
        for (int i = 0 ; i < tailleTabEntree - 1 ; i++) {
            if (tabEntree.get(i).isEmpty()) {
                String concat = tabEntree.get(i-1) + tabEntree.get(i+1);
                tabSortie.set(i - decalage, concat);
                i++;
                decalage = decalage + 2;
            } else {
                tabSortie.add(tabEntree.get(i));
            }
        }
        String[] tabSortieArray = new String[tabSortie.size()];

        return tabSortie.toArray(tabSortieArray);
    }

    public static String leftPadZeroAddOne(String valueStr) {
        StringBuilder retourStr = new StringBuilder();
        // on d?coupe la value en paquets de 4

        List<String> valueTab = new ArrayList<>();
        Integer nbTraitement = valueStr.length() / 4;
        Integer j = 0;
        for (int i = 0 ; i < nbTraitement ; i++) {
            valueTab.add(valueStr.substring(j, j + 4));
            j+=4;
        }

        Integer value = Integer.parseInt(valueTab.get(valueTab.size() - 1)) + 1;
        Integer nbZeroPad = 4 - value.toString().length();
        String lastValueStr = "";

        for (int i = 0 ; i < nbZeroPad ; i++) {
            lastValueStr += '0';
        }
        lastValueStr += value.toString();
        for (int i = 0 ; i < valueTab.size() - 1 ; i++) {
            retourStr.append(valueTab.get(i));
        }
        retourStr.append(lastValueStr);

        return "0001";
    }

    public static String creerLigne(List<String> nomColonneList, List<String> valeurList, String nouvelleValeur) {
        StringBuilder sb = new StringBuilder();

        sb.append("UPDATE COMM_MSG.T_SEX_SECTION_EXPORT SET ")
                .append("SEX_HIERARCHIE = '")
                .append(nouvelleValeur)
                .append("' WHERE ");
        for (int i = 0 ; i < nomColonneList.size() ; i++) {
            sb.append(nomColonneList.get(i));
            sb.append(" = '");
            sb.append(valeurList.get(i));
            sb.append("'");
            if (i != nomColonneList.size() - 1) {
                sb.append(" AND ");
            }
        }
        sb.append(";");
        sb.append("\n");
        return sb.toString();
    }

}

