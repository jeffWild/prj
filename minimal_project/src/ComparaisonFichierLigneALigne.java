import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by gsauvage on 24/05/2019.
 */
public class ComparaisonFichierLigneALigne {

    public ComparaisonFichierLigneALigne() {
    }

    private String cheminFichier1;

    private String cheminFichier2;

    public void comparerFichiers(String cheminFichier1, String cheminFichier2, String separateurLigne, String separarteurElement) {
        File fichier1 = new File(cheminFichier1);
        File fichier2 = new File(cheminFichier2);
        String cheminFichier3 = fichier1.getPath() + fichier1.getName() + "_" + fichier2.getName();
        String fichier1Str = GenerationEtLectureTexte.generateStringFromFile(fichier1);
        String fichier2Str = GenerationEtLectureTexte.generateStringFromFile(fichier2);
        StringBuilder fichier3Str = new StringBuilder();

        List<String> fichier1StrLignesList = Arrays.asList(fichier1Str.split(separateurLigne));
        List<String> fichier2StrLignesList = Arrays.asList(fichier2Str.split(separateurLigne));

        Iterator it1 = fichier1StrLignesList.iterator();
        while(it1.hasNext()) {
            String ligneFichier1 = (String) it1.next();
            Iterator it2 = fichier2StrLignesList.iterator();
            while (it2.hasNext()) {
                boolean ligneFichier1TrouveBln = false;
                String ligneFichier2 = (String) it2.next();
                if (ligneFichier1.toString().equals(ligneFichier2.toString())) {
                    fichier3Str.append(ligneFichier1 + " trouvée dans le fichier " + fichier1.getName() + " et " + fichier2.getName());
                    fichier3Str.append(separateurLigne);
                    ligneFichier1TrouveBln = true;
                }
                if (!it2.hasNext() && !ligneFichier1TrouveBln) {
                    fichier3Str.append(ligneFichier1 + " du fichier " + fichier1.getName() + " introuvable dans le fichier " + fichier2.getName());
                    fichier3Str.append(separateurLigne);
                }
            }
        }

        GenerationEtLectureTexte.generateFileFromString(fichier3Str.toString(), cheminFichier3);


    }


}
