import java.io.*;
import java.util.*;

/**
 * @author gsauvage
 */
public class GenererRequeteFromFile {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");

        File file = new File("D:/Profiles/gsauvage/Desktop/11099/donnees.txt");
        String texteEntier = GenerationEtLectureTexte.generateStringFromFile(file);
        String[] lignesTab = splitStringOnChar("\r\n", texteEntier);
        Set<Ligne> lignes = new HashSet<>();
        for (int i = 0 ; i < lignesTab.length ; i++) {
            String[] ligneTab = splitStringOnChar(";", lignesTab[i]);
            lignes.add(generateLigne(ligneTab));
        }
        GenerationEtLectureTexte.generateFileFromString(genererRequete(lignes), "D:/Profiles/gsauvage/Desktop/11099/fichier_resultat/requete.txt");

    }

    public static String genererRequete(Set<Ligne> lignes) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Ligne ligne : lignes) {
            sb.append("(");
            sb.append("DMA_CODE = '");
            sb.append(ligne.getDMA_CODE());
            sb.append("'");
            sb.append(" AND ");
            sb.append("FCA_CODE = '");
            sb.append(ligne.getFCA_CODE());
            sb.append("'");
            sb.append(" AND ");
            sb.append("ACA_CODE = '");
            sb.append(ligne.getACA_CODE());
            sb.append("'");
            sb.append(" AND ");
            sb.append("DNA_CODE = '");
            sb.append(ligne.getDNA_CODE());
            sb.append("'");
            sb.append(")");
            if (i == lignes.size() - 1) {
                sb.append(";");
            } else {
                sb.append(" OR ");
            }

            i++;

        }
        return sb.toString();
    }

    public static Ligne generateLigne(String[] ligneTab) {
        Ligne ligne = new Ligne();
        ligne.setDMA_CODE(ligneTab[0]);
        ligne.setDNA_CODE(ligneTab[3]);
        ligne.setFCA_CODE(splitStringOnChar(":",ligneTab[1])[0]);
        ligne.setACA_CODE(splitStringOnChar(":",ligneTab[2])[0]);
        return ligne;
    }


    public static String generateStringFromFile(String nomFichier) {
        String retour;
        try{
            File f = new File(nomFichier);
            byte[] buffer = new byte[(int)f.length()];
            DataInputStream in = new DataInputStream(new FileInputStream(f));
            in.readFully(buffer);
            in.close();
            retour = new String(buffer, "ISO-8859-15");
            return retour;
        } catch (FileNotFoundException e) {
            System.out.println("Impossible de lire le fichier "+nomFichier+" ! " +e);
            return "";
        } catch (IOException e) {
            System.out.println("Erreur de lecture !" +e);
            return "";
        }
    }

    public static File generateFileFromString(String content, String nomFichier) {
        File file = new File(nomFichier);

        try (FileOutputStream fop = new FileOutputStream(file)) {

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static String[] splitStringOnChar(String character, String textToSplit) {
        String[] textSplit = null;
        // vérifier que le caractère est bien un caractère de séparation
        textSplit = textToSplit.split(character);
        return textSplit;
    }

    private static class Ligne {
        private String FCA_CODE;
        private String ACA_CODE;
        private String DNA_CODE;
        private String DMA_CODE;

        public Ligne() {}

        public Ligne(String FCA_CODE, String ACA_CODE, String DNA_CODE, String DMA_CODE) {
            this.FCA_CODE = FCA_CODE;
            this.ACA_CODE = ACA_CODE;
            this.DNA_CODE = DNA_CODE;
            this.DMA_CODE = DMA_CODE;
        }

        public String getFCA_CODE() {
            return FCA_CODE;
        }

        public void setFCA_CODE(String FCA_CODE) {
            this.FCA_CODE = FCA_CODE;
        }

        public String getACA_CODE() {
            return ACA_CODE;
        }

        public void setACA_CODE(String ACA_CODE) {
            this.ACA_CODE = ACA_CODE;
        }

        public String getDNA_CODE() {
            return DNA_CODE;
        }

        public void setDNA_CODE(String DNA_CODE) {
            this.DNA_CODE = DNA_CODE;
        }

        public String getDMA_CODE() {
            return DMA_CODE;
        }

        public void setDMA_CODE(String DMA_CODE) {
            this.DMA_CODE = DMA_CODE;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Ligne ligne = (Ligne) o;

            if (Objects.equals(DMA_CODE, ligne.getDMA_CODE())
                    && Objects.equals(FCA_CODE, ligne.getFCA_CODE())
                    && Objects.equals(ACA_CODE, ligne.getACA_CODE())
                    && Objects.equals(DNA_CODE, ligne.getDNA_CODE())) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int result = FCA_CODE != null ? FCA_CODE.hashCode() : 0;
            result = 31 * result + (ACA_CODE != null ? ACA_CODE.hashCode() : 0);
            result = 31 * result + (DNA_CODE != null ? DNA_CODE.hashCode() : 0);
            result = 31 * result + (DMA_CODE != null ? DMA_CODE.hashCode() : 0);
            return result;
        }
    }

}


