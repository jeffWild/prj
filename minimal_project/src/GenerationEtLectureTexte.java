import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Classe de génération pour la lecture et l'écriture de fichiers textes
 * @author gsauvage
 */
public class GenerationEtLectureTexte {

    /**
     * Méthode permettant de générer un String à partir d'un fichier texte pour l'exploiter
     * @param file le fichier
     * @return le fichier en String
     */
    public static String generateStringFromFile(File file) {
        String retour;
        try{
            byte[] buffer = new byte[(int)file.length()];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            in.readFully(buffer);
            in.close();
            retour = new String(buffer, "ISO-8859-15");
            return retour;
        } catch (FileNotFoundException e) {
            System.out.println("Impossible de lire le fichier "+file.getAbsolutePath()+" ! " +e);
            return "";
        } catch (IOException e) {
            System.out.println("Erreur de lecture !" +e);
            return "";
        }
    }

    /**
     * Méthode permettant de générer un fichier avec son contenu au format String
     * @param content
     * @param nomFichier
     * @return le fichier File
     */
    public static File generateFileFromString(String content, String nomFichier) {
        File file = new File(nomFichier);

        try (FileOutputStream fop = new FileOutputStream(file)) {

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static String[] splitStringOnChar(String character, String textToSplit) {
        String[] textSplit = null;
        // vérifier que le caractère est bien un caractère de séparation
        textSplit = textToSplit.split(character);
        return textSplit;
    }

    public static File nettoyerFichier(String cheminFichierANettoyer) {
        String fichierANettoyerStr = generateStringFromFile(new File(cheminFichierANettoyer));
        List<String> fichierANettoyerStrList = Arrays.asList(fichierANettoyerStr.split("\n"));

        StringBuilder sb = new StringBuilder();
        Iterator it = fichierANettoyerStrList.iterator();
        while(it.hasNext()) {
            String ligne = (String) it.next();
            if (!ligne.contains("ok pour") && !ligne.contains("erreur meme date")) {
                sb.append(ligne);
                sb.append("\r\n");
            }
        }

        return generateFileFromString(sb.toString(), "F:/fichierNettoye.txt");
    }

    public static void exploitationFichierNettoye(String cheminFichier, String cheminSortieErreurInconnue, String cheminSortieErreurInterne) {
        String fichierStr = generateStringFromFile(new File(cheminFichier));
        List<String> fichierStrList = Arrays.asList(fichierStr.split("\r\n"));
        Map<String, Integer> mapErreurInconnue = new HashMap<>();
        Map<String, Integer> mapErreurInterne = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYY HH:MM");
        Iterator it = fichierStrList.iterator();
        while (it.hasNext()) {
            String ligne = (String) it.next();
            String[] ligneSplit = ligne.split(" ");
            if (ligneSplit.length == 7) {
                // erreur inconnue
                String dateHoraire = ligneSplit[5] + " " + ligneSplit[6];
                ajouterMap(mapErreurInconnue, dateHoraire);
            } else if (ligneSplit.length == 8) {
                // erreur interne
                String dateHoraire = ligneSplit[6] + " " + ligneSplit[7];
                ajouterMap(mapErreurInterne, dateHoraire);
            }
        }

        CSVEditor.writeDataLineByLine(cheminSortieErreurInconnue, mapErreurInconnue);
        CSVEditor.writeDataLineByLine(cheminSortieErreurInterne, mapErreurInterne);
    }

    private static void ajouterMap(Map<String, Integer> map, String dateHoraire) {
        if (map.containsKey(dateHoraire)) {
            Integer value = map.get(dateHoraire);
            map.remove(dateHoraire);
            value = value + 1;
            map.put(dateHoraire, value);
        } else {
            map.put(dateHoraire, 1);
        }
    }

}
