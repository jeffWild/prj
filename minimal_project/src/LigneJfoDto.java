import com.sun.deploy.util.StringUtils;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Dto pour la ligne de la T_JFO
 * @author gsauvage
 */
public class LigneJfoDto {

    private String FCA_CODE;
    private String ACA_CODE;
    private String DNA_CODE;
    private String DMA_CODE;

    public LigneJfoDto() {}

    public LigneJfoDto(String FCA_CODE, String ACA_CODE, String DNA_CODE, String DMA_CODE) {
        this.FCA_CODE = FCA_CODE;
        this.ACA_CODE = ACA_CODE;
        this.DNA_CODE = DNA_CODE;
        this.DMA_CODE = DMA_CODE;
    }

    public String getFCA_CODE() {
        return FCA_CODE;
    }

    public void setFCA_CODE(String FCA_CODE) {
        this.FCA_CODE = FCA_CODE;
    }

    public String getACA_CODE() {
        return ACA_CODE;
    }

    public void setACA_CODE(String ACA_CODE) {
        this.ACA_CODE = ACA_CODE;
    }

    public String getDNA_CODE() {
        return DNA_CODE;
    }

    public void setDNA_CODE(String DNA_CODE) {
        this.DNA_CODE = DNA_CODE;
    }

    public String getDMA_CODE() {
        return DMA_CODE;
    }

    public void setDMA_CODE(String DMA_CODE) {
        this.DMA_CODE = DMA_CODE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LigneJfoDto ligneTJfo = (LigneJfoDto) o;

        if (Objects.equals(DMA_CODE, ligneTJfo.getDMA_CODE())
                && Objects.equals(FCA_CODE, ligneTJfo.getFCA_CODE())
                && Objects.equals(ACA_CODE, ligneTJfo.getACA_CODE())
                && Objects.equals(DNA_CODE, ligneTJfo.getDNA_CODE())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = FCA_CODE != null ? FCA_CODE.hashCode() : 0;
        result = 31 * result + (ACA_CODE != null ? ACA_CODE.hashCode() : 0);
        result = 31 * result + (DNA_CODE != null ? DNA_CODE.hashCode() : 0);
        result = 31 * result + (DMA_CODE != null ? DMA_CODE.hashCode() : 0);
        return result;
    }

    public static LigneJfoDto genererLigneFromTab(String[] tableau) {
        LigneJfoDto LigneJfoDto = new LigneJfoDto();
        LigneJfoDto.setDMA_CODE(tableau[0]);
        LigneJfoDto.setFCA_CODE(tableau[1]);
        LigneJfoDto.setACA_CODE(tableau[2]);
        LigneJfoDto.setDNA_CODE(tableau[3]);
        return LigneJfoDto;
    }

    public static Set<LigneJfoDto> genererLignesFromTexte(String texte){
        Set<LigneJfoDto> ligneJfoDtoSet = new HashSet<>();

        String[] tableau = GenerationEtLectureTexte.splitStringOnChar("\r\n", texte);
        for (String ligne : tableau) {
            String[] ligneDecoupeTab = GenerationEtLectureTexte.splitStringOnChar("\t", ligne);
            ligneJfoDtoSet.add(LigneJfoDto.genererLigneFromTab(ligneDecoupeTab));
        }
        return ligneJfoDtoSet;
    }

    public static String genererTexteFromLigneSet(Set<LigneJfoDto> LigneJfoDtoSet) {
        StringBuilder sb = new StringBuilder();
        for (LigneJfoDto LigneJfoDto : LigneJfoDtoSet) {
            sb.append(LigneJfoDto.getDMA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getFCA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getACA_CODE());
            sb.append(" ; ");
            sb.append(LigneJfoDto.getDNA_CODE());
            sb.append("\r\n");
        }
        return sb.toString();
    }
}
