import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by gsauvage on 03/06/2019.
 */
public class CSVEditor {


    public static void writeDataLineByLine(String filePath, Map<String, Integer> map)
    {
        // first create file object for file placed at location
        // specified by filepath
        File file = new File(filePath);
        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            // adding header to csv
            String[] header = { "date", "compteur" };
            writer.writeNext(header);

            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                String date = entry.getKey();
                Integer compteur = entry.getValue();
                String[] data = { date, compteur.toString() };
                writer.writeNext(data);
            }
            writer.close();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
