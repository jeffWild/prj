/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('codingDojoAngularJs')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
