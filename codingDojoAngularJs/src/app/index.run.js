(function() {
  'use strict';

  angular
    .module('codingDojoAngularJs')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
